class RestaurantsController < ApplicationController
  def index
    @restaurants = Restaurant.all
  end

  def show
    @restaurant = Restaurant.find(params[:id])
  end

  def new
    @restaurant = Restaurant.new
  end

  def create
    @restaurant = Restaurant.new(name: params[:restaurant][:name], address: params[:restaurant][:address], phone_number: params[:restaurant][:phone_nubmer], category: params[:restaurant][:category][0])
    @restaurant.save
    redirect_to root_path
  end

  def edit
    @restaurant = Restaurant.find(params[:id])
  end

  def update
    @restaurant = Restaurant.find(params[:id])
    @restaurant.update(name: params[:restaurant][:name], address: params[:restaurant][:address], phone_number: params[:restaurant][:phone_nubmer], category: params[:restaurant][:category][0])
    redirect_to restaurant_path(@restaurant)
  end

  def destroy
    @restaurant = Restaurant.find(params[:id])
    @restaurant.destroy
    redirect_to root_path
  end

end
