class Review < ApplicationRecord
  belongs_to :restaurant
  validates :restaurant_id, presence: true
  validates :rating, inclusion: { in: [1, 2, 3, 4, 5] }
  validates :content, presence: true
end
