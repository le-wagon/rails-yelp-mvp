# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)


Restaurant.create(name: "Au bon grec", address: "Villeneuve-st-georges", phone_number: "0678379376" ,category: "french")
Restaurant.create(name: "Le reveil des sens", address: "Paradis", phone_number: "0678379376" ,category: "italian")
Restaurant.create(name: "L'Alicheur", address: "Oberkampf", phone_number: "0678379376" ,category: "japanese")
Restaurant.create(name: "Pizza hut", address: "Marseille", phone_number: "0678379376" ,category: "italian")
Restaurant.create(name: "Chez Jimmy", address: "Belleville", phone_number: "0678379376" ,category: "chinese")
Restaurant.create(name: "Fricadelle", address: "Bruxelle", phone_number: "0678379376" ,category: "belgian")

